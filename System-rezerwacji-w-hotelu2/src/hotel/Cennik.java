/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hotel;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.StringTokenizer;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author dd
 */
class TypeOfRoom{
    int type;
    int price;
    int numOfPersons;
    
    public TypeOfRoom(int ty,int num,int pr){
        this.type = ty;
        this.price = pr;
        this.numOfPersons = num;
    }
}

class Element{
    Date begin;
    Date end;
    List<TypeOfRoom> rooms;
    
    public Element(Date be,Date en,List<TypeOfRoom> ro){
        this.begin = be;
        this.end = en;
        this.rooms = ro;
    }

}
class Cennik {
    
    List<Element> data ;
    String dir = "src/Ceny/";
    

    Cennik() {
        this.data = new ArrayList<>();
    }
    Cennik(String dir) {
        this.data = new ArrayList<>();
        this.dir = dir;
    }

    public void importData(){
        try {
            Files.walk(Paths.get(dir)).forEach((Path filePath) -> {
                if (Files.isRegularFile(filePath)) {
                    String[] parts = filePath.getFileName().toString().split("-");
                    SimpleDateFormat formater = new SimpleDateFormat("dd.MM");
                    try
                    {
                        Date begin = (Date)formater.parse(parts[0]);
                        Date end = (Date)formater.parse(parts[1]);
                        BufferedReader input = new BufferedReader(new FileReader(filePath.toString()));
                        String line,token;
                        List<TypeOfRoom> tor = new ArrayList<>();
                        while ((line = input.readLine()) != null){
                            
                            if (line.trim().length() == 0)
                                continue;

                            StringTokenizer tokenizer= new StringTokenizer(line, "|");
                            token= tokenizer.nextToken().trim();
                            int ty= Integer.parseInt(token);
                            token= tokenizer.nextToken().trim();
                            int numOfPersson= Integer.parseInt(token);
                            token= tokenizer.nextToken().trim();
                            int price= Integer.parseInt(token);
                            tor.add(new TypeOfRoom(ty,numOfPersson,price));                         
                        }
                        data.add(new Element(begin,end,tor));
                    }
                    catch (ParseException | IOException | NumberFormatException ex)
                    {
                        data =null;
//                         Logger.getLogger(Cennik.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }
            });     
        } catch (Exception ex) {
            Logger.getLogger(Cennik.class.getName()).log(Level.SEVERE, null, ex);
        }
    } 
    
    
}

class Start {
    public static void main(String[] args) {
        // TODO code application logic here
        Cennik cen = new Cennik();
        cen.importData();
    }
}
