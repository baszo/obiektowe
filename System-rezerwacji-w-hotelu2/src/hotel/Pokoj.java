/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hotel;

/**
 *
 * @author dd
 */
public class Pokoj {
    private final String name;
    private final int roomNumber;
    private final int numberOfBeds;
    private final char typeOfBed;
    private final int classOfRoom;
    
    public Pokoj(String na,int rn,int nob,char tob,int idclass)
    {
        this.name=na;
        this.roomNumber=rn;
        this.numberOfBeds=nob;
        this.typeOfBed=tob;
        this.classOfRoom=idclass;
    }
    
}
