/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hotel;

import static com.sun.org.apache.regexp.internal.RETest.test;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.StringTokenizer;
import static jdk.nashorn.internal.objects.NativeRegExp.test;

/**
 *
 * @author dd
 */
public class KlasyPokojow {
    Map<Integer,String> klasy = new HashMap<>() ;
    
    public void importData() throws FileNotFoundException, IOException{
        BufferedReader input = new BufferedReader(new FileReader("src/Klasy/klasy.dat"));
        String line;
        try{

            while ((line = input.readLine()) != null){

                if (line.trim().length() == 0)
                    continue;

                StringTokenizer tokenizer= new StringTokenizer(line, "|");
                int id = Integer.parseInt(tokenizer.nextToken().trim());
                String name = tokenizer.nextToken().trim();
                klasy.put(id, name);
            }
        }
        catch(Exception ex){
            System.out.println(ex);
        }
        
    }
}

class Start {
    public static void main(String[] args) {
        // TODO code application logic here
        KlasyPokojow kl = new KlasyPokojow();
        try
        {
            kl.importData();
            kl.klasy.keySet().stream().forEach((key) -> {
                System.out.println(key + " " + kl.klasy.get(key));
            });
        }
        catch(Exception ex){

        }
    }
}

