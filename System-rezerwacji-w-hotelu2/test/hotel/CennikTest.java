/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hotel;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.text.SimpleDateFormat;
import java.util.Date;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.rules.ExpectedException;
import org.junit.rules.TemporaryFolder;

/**
 *
 * @author dd
 */
public class CennikTest {
    
    public TemporaryFolder temporaryFolder = new TemporaryFolder();

    public ExpectedException thrown = ExpectedException.none();
    
    public CennikTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {

    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of importData method, of class Cennik.
     */
    @Test
    public void testImportData() {
        System.out.println("importData");
        BufferedWriter output = null;
        try
        {
            File file = new File("test/resources/CennikData/1.11-2.10");
            output = new BufferedWriter(new FileWriter(file));
            String text = "1|2|3\n"
                    + "2|2|2\n"
                    + "3|3|4\n";
            output.write(text);
            if ( output != null ) output.close();
            Cennik cen = new Cennik("test/resources/CennikData");
            cen.importData();
            SimpleDateFormat formater = new SimpleDateFormat("dd.MM");
            for(Element c : cen.data)
            {
                assertEquals(c.begin,(Date)formater.parse("1.11"));
                assertEquals(c.end,(Date)formater.parse("2.10"));
                
                String rooms="";
                for(TypeOfRoom typ : c.rooms)
                {
                    rooms+=typ.type+"|"+typ.numOfPersons+"|"+typ.price+"\n";
                }
                assertEquals(rooms,text);
            }
        // TODO review the generated test code and remove the default call to fail.
        }
        catch (Exception ex)
        {
            fail(ex.toString());
        }
    }
    
    @Test
    public void testWrongFilesFormatImportData() throws FileNotFoundException {
        System.out.println("checkFiles");
        File file = new File("test/resources/CennikData/test.txt");
        
        Cennik cen = new Cennik("test/resources/CennikData");
        Exception ex = null;
        try
        {
            file.createNewFile();
            cen.importData();
            assertEquals(null,cen.data);
        }
        catch(Exception e){
            fail(ex.toString());
        }
        if(file != null)
            System.out.println(file.delete());
        
    }
    
}
