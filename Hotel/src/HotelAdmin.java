import services.GuestsService;
import services.RoomService;

import java.util.Scanner;

public class HotelAdmin {

    Scanner in = new Scanner(System.in);
    String menu = "Panel admina hotelu XYZ \n" +
            "prosze wybrac jedna z opcji: \n" +
            "1) wyszukaj rezerwacje\n" +
            "2) usun rezerwacje\n" +
            "3) wyswietl informacje o rezerwacji\n" +
            "4) wyswietl informacje o gosciach hotelowych\n" +
            "5) dodaj zniżkę dla klienta\n" +
            "q) aby wyjść";

    public void findReservation(){

    }

    public void deleteReservation(){

    }

    public void showReservation(){

    }

    public void guestsOfHotel(){

    }

    public void addDiscount(){
        String email;
        GuestsService guestsService = new GuestsService();

//        try {
//            email = in.nextLine();
//            guestsService.find_by_email(email);
//        }
    }

    public void run() {
        String input = "";

        try {
            do {

                System.out.println(menu);
                input = in.nextLine();
                switch (input) {
                    case "1": findReservation(); break;
                    case "2": deleteReservation(); break;
                    case "3": showReservation();
                        break;
                    case "4": guestsOfHotel(); break;
                    case "5": addDiscount(); break;
                }

            }
            while (input.compareTo("q") != 0);
        }
        catch(Exception e)
        {

        }

    }

}


class AdminStart{
    public static void main(String[] args) {
        HotelAdmin hotel = new HotelAdmin();
        hotel.run();
    }
}