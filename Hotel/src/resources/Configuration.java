package resources; /**
 * Created by andrzej2 on 09.11.15.
 */
import java.util.*;
import java.io.*;

//klasa odpowiadająca za ładowanie konfiguracji
public class Configuration {
    private static Configuration _instance = null;

    private Properties props = null;

    private Configuration() {
        props = new Properties();
        try {
            FileInputStream fis = new FileInputStream(new File("src/config.properties"));
            props.load(fis);
        }
        catch (Exception e) {
            // catch resources.Configuration Exception right here
        }
    }

    public synchronized static Configuration getInstance() {
        if (_instance == null)
            _instance = new Configuration();
        return _instance;
    }

    // get property value by name
    public String getProperty(String key) {
        String value = null;
        if (props.containsKey(key))
            value = (String) props.get(key);
        else {
            // property brak
        }
        return value;
    }
}