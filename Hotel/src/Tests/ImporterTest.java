package Tests;

import model.Discount;
import org.junit.Rule;
import org.junit.Test;

import java.io.BufferedWriter;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Map;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

/**
 * Created by andrzej2 on 26.10.15.
 */
public class ImporterTest {

    public model.Importer imp = new model.Importer();

    @Test
    public void testImportCsvDataPricingData() throws Exception {
        System.out.println("importData");
        try
        {
            String text = "0|100\n" +
                    "1|200\n" +
                    "2|100\n" +
                    "3|100\n" +
                    "4|100\n" +
                    "5|100\n" +
                    "6|100\n" +
                    "7|100\n" +
                    "8|100\n" +
                    "9|100\n" +
                    "10|100\n" +
                    "11|100\n" +
                    "12|100\n" +
                    "13|100\n" +
                    "14|100\n" +
                    "15|100";


            List<model.Pricing> data =  imp.importCsvDataPricingData();
            SimpleDateFormat formater = new SimpleDateFormat("dd.MM.yyyy");
            String rooms = "";
            for(model.Pricing c : data) {
                assertEquals(c.getBegin(),  formater.parse("1.1.2015"));
                assertEquals(c.getEnd(),  formater.parse("31.12.2015"));



                for (Map.Entry<Integer, Integer> entry : c.getRooms().entrySet()) {
                    rooms += entry.getKey() + "|" + entry.getValue() + "\n";
                }
            }
            assertEquals(rooms, text +"\n");
            // TODO review the generated test code and remove the default call to fail.
        }
        catch (Exception ex)
        {
            fail(ex.toString());
        }

    }

    @Test
    public void testImporClassRoomsData() throws Exception {

    }

    @Test
    public void testImportRoomsFromCsv() throws Exception {

    }

    @Test
    public void testImportDiscountFromCsv() throws Exception {
        String text = "1|P|5|Stałego klienta\n" +
                "2|A|50|Wczesna rezerwacja\n" +
                "3|A|20|Pierwsza rezerwacja";
        String discount = "";
        List<Discount> diss = imp.importDiscount();
        for(model.Discount c : diss) {
            discount+=c.toString()+ "\n";
        }
        assertEquals(text + "\n", discount);

    }
}