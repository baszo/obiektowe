package Tests;

import org.junit.Assert;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TemporaryFolder;

import java.io.File;

import static org.junit.Assert.*;
import static org.junit.Assert.assertEquals;

/**
 * Created by andrzej2 on 15.11.15.
 */
public class EksporterTest {

    @Rule
    public TemporaryFolder folder = new TemporaryFolder();

    @Test
    public void testInsertReservationsToCsv() throws Exception {
    }

    @Test
    public void testUpdateReservationToCsv() throws Exception {

    }

    @Test
    public void testInsertRoomToCsv() throws Exception {
        File createdFile = folder.newFile("testnewseroed");
        model.Eksporter ex = new model.Eksporter();
        model.ClassOfRoom cor = new model.ClassOfRoom(1,3,"Vip");
        model.Room room = new model.Room(107,"S",cor);
        ex.insertNewRecord(createdFile.getAbsolutePath(),room.toString());
        assertEquals(ex.chechIfLineExist(createdFile.getAbsolutePath(),"107|S|1"),true);

    }

    @Test
    public void testUpdateRoomToCsv() throws Exception {
        File createdFile = folder.newFile("testnewseroed");
        model.Eksporter ex = new model.Eksporter();
        model.ClassOfRoom cor = new model.ClassOfRoom(1,3,"Vip");
        model.Room room = new model.Room(107,"S",cor);
        model.Room roomNew = new model.Room(207,"S",cor);
        ex.insertNewRecord(createdFile.getAbsolutePath(),room.toString());
        ex.updateRecord(createdFile.getAbsolutePath(), room.toString(), roomNew.toString());
        assertEquals(ex.chechIfLineExist(createdFile.getAbsolutePath(),"207|S|1"),true);


    }

    @Test
    public void testInsertNewRecord() throws Exception {
        File createdFile = folder.newFile("testnewseroed");
        model.Eksporter ex = new model.Eksporter();
        ex.insertNewRecord(createdFile.getAbsolutePath(),"3|A|20|Pierwsza a");
        assertEquals(ex.chechIfLineExist(createdFile.getAbsolutePath(),"3|A|20|Pierwsza a"),true);

    }
    @Test
    public void testDelateNewRecord() throws Exception {
        File createdFile = folder.newFile("testnewseroed");
        model.Eksporter ex = new model.Eksporter();
        ex.insertNewRecord(createdFile.getAbsolutePath(),"3|A|20|Pierwsza a");
        ex.deleteRecord(createdFile.getAbsolutePath(), "3|A|20|Pierwsza a");
        assertEquals(ex.chechIfLineExist(createdFile.getAbsolutePath(), "3|A|20|Pierwsza a"), false);
    }

    @Test
    public void testChechIfLineExist() throws Exception {
        File createdFile = folder.newFile("testnewseroed");
        model.Eksporter ex = new model.Eksporter();
        ex.insertNewRecord(createdFile.getAbsolutePath(),"3|A|20|Pierwsza a");
        assertEquals(ex.chechIfLineExist(createdFile.getAbsolutePath(), "3|A|20|Pierwsza a"), true);
        assertEquals(ex.chechIfLineExist(createdFile.getAbsolutePath(),"3|A|20|Pierwsza"),false);

    }

    @Test
    public void testUpdateRecord() throws Exception {
        File createdFile = folder.newFile("testnewseroed");
        model.Eksporter ex = new model.Eksporter();
        ex.insertNewRecord(createdFile.getAbsolutePath(),"3|A|20|Pierwsza a");
        ex.updateRecord(createdFile.getAbsolutePath(), "3|A|20|Pierwsza a", "3|A|20|Pierwsza b");
        assertEquals(ex.chechIfLineExist(createdFile.getAbsolutePath(),"3|A|20|Pierwsza b"),true);

    }
}