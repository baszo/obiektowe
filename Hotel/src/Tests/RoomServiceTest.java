package Tests;

import org.junit.Test;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static org.junit.Assert.*;
import static org.junit.Assert.assertEquals;

/**
 * Created by andrzej2 on 15.11.15.
 */
public class RoomServiceTest {

    public services.RoomService roomService = new services.RoomService();

    @Test
    public void testGetHotelResources() throws Exception {

    }

    @Test
    public void testGetRoomsAvalibleForClassId() throws Exception {
        List<Integer> test= roomService.getRoomsAvalibleForClassId(new Date(),new Date(),3);
        List<Integer> argumentComponents = new ArrayList<>();
        int x = 107;
        argumentComponents.add(x);
        assertEquals(test,argumentComponents);

    }

    @Test
    public void testGetAllClases() throws Exception {
        String test = roomService.getAllClases();
        String eq = "1) Pokój Prezydencki 1 osobowy\n" +
                "2) Pokój Vip 1 osobowy\n" +
                "3) Pokój Economy 1 osobowy\n" +
                "4) Pokój Standart 2 osobowy\n" +
                "5) Pokój Prezydencki 2 osobowy\n" +
                "6) Pokój Vip 2 osobowy\n" +
                "7) Pokój Economy 2 osobowy\n" +
                "8) Pokój Standart 3 osobowy\n" +
                "9) Pokój Prezydencki 3 osobowy\n" +
                "10) Pokój Vip 3 osobowy\n" +
                "11) Pokój Economy 3 osobowy\n" +
                "12) Pokój Standart 4 osobowy\n" +
                "13) Pokój Prezydencki 4 osobowy\n" +
                "14) Pokój Vip 4 osobowy\n" +
                "15) Pokój Economy 4 osobowy";
        assertEquals(test, eq +"\n");

    }
}