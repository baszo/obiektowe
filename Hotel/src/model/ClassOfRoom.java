package model;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author dd
 */
public class ClassOfRoom {
    int Id;
    String description;
    int numOfPersons;

    public int getId() {
        return Id;
    }

    public String getDescription() {
        return description;
    }


    public int getNumOfPersons() {
        return numOfPersons;
    }


    public ClassOfRoom(int id,int num,String description){
        this.Id=id;
        this.numOfPersons = num;
        this.description = description;
    }

    public String toString(){
        return "Pokój "+description + " " + numOfPersons + " osobowy";
    }

}

