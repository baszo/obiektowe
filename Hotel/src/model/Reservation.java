package model;

import java.util.Date;
import java.util.Map;

public class Reservation {
    private final Guest guestId;
    private final Room room;
    private final Date start_at;
    private final Date end_at;

    @Override
    public String toString() {
        return guestId +
                "|" + room +
                "|" + start_at +
                "|" + end_at;
    }

    public Reservation(Guest guestId, Room room, Date start_at, Date end_at){
        this.guestId = guestId;
        this.room = room;
        this.start_at = start_at;
        this.end_at = end_at;
    }

    public Date getBegin() {
        return start_at;
    }

    public Date getEnd() {
        return end_at;
    }

    public Room getRoom(){
        return room;
    }

}
