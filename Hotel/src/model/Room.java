package model;

import java.util.List;

/**
 * Created by andrzej2 on 19.10.15.
 */
public class Room {
    public int getRoomNumber() {
        return roomNumber;
    }

    public int getNumOfPersons() {
        return classOfRoom.getNumOfPersons();
    }

    public String getTypeOfBed() {
        return typeOfBed;
    }

    public ClassOfRoom getClassOfRoom() {
        return classOfRoom;
    }

    private final int roomNumber;
    private final String typeOfBed;
    private final ClassOfRoom classOfRoom;

    public Room( int rn, String tob, ClassOfRoom idclass)
    {
        this.roomNumber=rn;
        this.typeOfBed=tob;
        this.classOfRoom=idclass;
    }

    public String toString(){
        return roomNumber+"|"+typeOfBed+"|"+classOfRoom.getId();
    }

}