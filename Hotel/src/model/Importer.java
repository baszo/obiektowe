package model;

import resources.Configuration;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Importer {

    //import pricing data from csv
    public List<Pricing> importCsvDataPricingData()
    {
        String dir = Configuration.getInstance().getProperty("pricesPath");
        final List<Pricing> data =  new ArrayList<>();
        try {
            Files.walk(Paths.get(dir)).forEach((Path filePath) -> {
                if (Files.isRegularFile(filePath)) {
                    String[] parts = filePath.getFileName().toString().split("-");
                    SimpleDateFormat formater = new SimpleDateFormat("dd.MM.yyyy");
                    try
                    {
                        Date begin = formater.parse(parts[0]);
                        Date end = formater.parse(parts[1]);
                        BufferedReader input = new BufferedReader(new FileReader(filePath.toString()));
                        String line,token;
                        Map<Integer,Integer> tor = new HashMap<>();
                        while ((line = input.readLine()) != null){

                            if (line.trim().length() == 0)
                                continue;

                            StringTokenizer tokenizer= new StringTokenizer(line, "|");
                            token= tokenizer.nextToken().trim();
                            int id= Integer.parseInt(token);
                            token= tokenizer.nextToken().trim();
                            int price= Integer.parseInt(token);
                            tor.put(id,price);
                        }
                        data.add(new Pricing(begin, end, tor));
                    }
                    catch (ParseException | IOException | NumberFormatException ex)
                    {
//                         Logger.getLogger(Cennik.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }
            });
        } catch (Exception ex) {
            Logger.getLogger(Pricing.class.getName()).log(Level.SEVERE, null, ex);
        }
        return data;
    }


    //import class of rooms data from csv
    public List<ClassOfRoom> imporClassRoomsData()
    {
        List<ClassOfRoom> klasy = new ArrayList<>() ;
        try{
        BufferedReader input = new BufferedReader(new FileReader(Configuration.getInstance().getProperty("classPath")));
        String line,token;

            while ((line = input.readLine()) != null){

                if (line.trim().length() == 0)
                    continue;

                StringTokenizer tokenizer= new StringTokenizer(line, "|");

                token= tokenizer.nextToken().trim();
                int id= Integer.parseInt(token);

                token= tokenizer.nextToken().trim();
                int size= Integer.parseInt(token);

                String description = tokenizer.nextToken().trim();
                klasy.add(new ClassOfRoom(id,size, description));
            }
        }
        catch(Exception ex){
            System.out.println(ex);
        }
        return klasy;
    }

    //import pokoi hotelowych
    public List<Room> importRoomsFromCsv()
    {

        List<Room> rooms = new ArrayList<>();
        List<ClassOfRoom> classes = imporClassRoomsData();
        List<Pricing> prices = importCsvDataPricingData();
        try {
            BufferedReader input = new BufferedReader(new FileReader(Configuration.getInstance().getProperty("roomPath")));
            String line;
            while ((line = input.readLine()) != null){

                if (line.trim().length() == 0)
                    continue;

                StringTokenizer tokenizer= new StringTokenizer(line, "|");
                int roomNumber = Integer.parseInt(tokenizer.nextToken().trim());
                String typeOfBed = tokenizer.nextToken().trim();
                int classOfRoom = Integer.parseInt(tokenizer.nextToken().trim());
                ClassOfRoom clas = null;
                for(ClassOfRoom cl : classes)
                {
                    if(cl.Id == classOfRoom)
                        clas = cl;
                }
                if(clas == null) throw new Exception("no room klas");
                rooms.add(new Room(roomNumber,typeOfBed,clas));
            }
        }
        catch(Exception e)
        {
            Logger.getLogger(Pricing.class.getName()).log(Level.SEVERE, null, e);
        }

        return rooms;

    }

    //import zniżek
    public List<Discount> importDiscount()
    {
        List<Discount> disco = new ArrayList<>() ;
        try{
            BufferedReader input = new BufferedReader(new FileReader(Configuration.getInstance().getProperty("discountPath")));
            String line,token;

            while ((line = input.readLine()) != null){

                if (line.trim().length() == 0)
                    continue;

                StringTokenizer tokenizer= new StringTokenizer(line, "|");

                token= tokenizer.nextToken().trim();
                int id= Integer.parseInt(token);
                String type = tokenizer.nextToken().trim();
                int amount= Integer.parseInt(tokenizer.nextToken().trim());
                String description = tokenizer.nextToken().trim();

                disco.add(new Discount(id,type.charAt(0), description,amount));
            }
        }
        catch(Exception ex){
            System.out.println(ex);
        }
        return disco;
    }


    public List<Reservation> importReservationsFromCsv()
    {

        List<Reservation> reservations = new ArrayList<>();
        return reservations;

    }


}

class Start {
    public static void main(String[] args) {
        // TODO code application logic here
        Importer kl = new Importer();
        List<Room> rooms = new ArrayList<>();
        try
        {
            rooms = kl.importRoomsFromCsv();
        }
        catch(Exception ex){

        }
        for(Room t : rooms){
            System.out.println(t.toString());
        }
    }
}

