package model;

import java.io.*;
import resources.Configuration;

public class Eksporter {
    public boolean insertReservationsToCsv(Reservation reservation){

        return insertNewRecord(Configuration.getInstance().getProperty("reservationPath"),reservation.toString());
    }
    public boolean deleteReservationsToCsv(Reservation reservation){

        return deleteRecord(Configuration.getInstance().getProperty("reservationPath"),reservation.toString());
    }

    public boolean updateReservationToCsv(Reservation reservationOld,Reservation reservationNew)
    {
        return updateRecord(Configuration.getInstance().getProperty("reservationPath"), reservationOld.toString(), reservationNew.toString());
    }

    public boolean insertRoomToCsv(Room room){

        return insertNewRecord(Configuration.getInstance().getProperty("reservationPath"), room.toString());
    }

    public boolean updateRoomToCsv(Room roomOld,Room roomNew)
    {
        return updateRecord(Configuration.getInstance().getProperty("reservationPath"), roomOld.toString(), roomNew.toString());
    }


    //dodanie do podanego pliku nowego rekordu
    public boolean insertNewRecord(String file,String Record)
    {
        try
        {
            if (!chechIfLineExist(file, Record))
            {
                BufferedWriter input = new BufferedWriter(new FileWriter(file,true));
                input.append("\n"+Record);
                input.close();
            }
            else
            {
                return false;
            }
        }
        catch(Exception e)
        {

        }
        return true;
    }


    //sprawdzenie czy linia już istnieje
    public boolean chechIfLineExist(String path,String lineCheck)
    {
        try {
            BufferedReader input = new BufferedReader(new FileReader(path));

            //now read the file line by line...
            String line;
            while ((line = input.readLine()) != null){
                if(line.equals(lineCheck) ) {
                    return true;
                }
            }
        } catch(Exception e) {
            //handle this
            System.out.println(e);
        }
        return false;
    }

    //aktualizuje w podanym pliku rekord
    public boolean updateRecord(String file,String oldRecord,String newRecord)
    {
        BufferedReader reader = null;
        boolean isRecord = false;
        try {
            reader = new BufferedReader(new FileReader(file));

            String line = "", newtext = "";
            while((line = reader.readLine()) != null)
            {
                if(line.contains(oldRecord))
                {
                    newtext += newRecord + "\n";
                    isRecord = true;
                }
                else
                    newtext+=line + "\n";
            }
            reader.close();
            if(isRecord) {
                FileWriter writer = new FileWriter(file);
                writer.write(newtext);
                writer.close();
            }
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }

        return isRecord;
    }
    //aktualizuje w podanym pliku rekord
    public boolean deleteRecord(String file,String record)
    {
        BufferedReader reader = null;
        boolean isRecord = false;
        try {
            reader = new BufferedReader(new FileReader(file));

            String line = "", newtext = "";
            while((line = reader.readLine()) != null)
            {
                if(line.equals(record)) {
                    isRecord = true;
                }
                else
                    newtext+=line + "\n";
            }
            reader.close();
            if(isRecord) {
                FileWriter writer = new FileWriter(file);
                writer.write(newtext);
                writer.close();
            }
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }

        return isRecord;
    }
}

class test{
    public static void main(String[] args) {
        Eksporter tst = new Eksporter();
        System.out.println(tst.updateRecord(Configuration.getInstance().getProperty("discountPath"), "3|A|20|Pierwsza a", "3|A|20|Pierwsza dupa"));
    }
}
