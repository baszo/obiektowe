package model;

import java.util.Date;
import java.util.Map;


public class Pricing {

    public Date getBegin() {
        return begin;
    }

    public Date getEnd() {
        return end;
    }

    public Map<Integer,Integer> getRooms() {
        return roomsPrices;
    }

    Date begin;
    Date end;
    Map<Integer,Integer> roomsPrices;

    public Pricing(Date be,Date en,Map<Integer,Integer> ro){
        this.begin = be;
        this.end = en;
        this.roomsPrices = ro;
    }

}
