package model;

/**
 * Created by andrzej2 on 09.11.15.
 */
public class Discount {
    int id;
    char type;
    int amount;
    String name;

    public Discount(int id, char type, String name, int amount) {
        this.id = id;
        this.type = type;
        this.name = name;
        this.amount = amount;
    }

    public String toString() {
        return id+"|"+type+"|"+amount+"|"+name;
    }
}
