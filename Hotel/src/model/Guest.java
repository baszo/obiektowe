package model;

import java.util.Date;

public class Guest {
    private Guest guestId;
    private String name;
    private String email;

    public Guest(String name, String email){
        this.name = name;
        this.email = email;
    }

    public String toString(){
        return "Imie " + name + ", email " + email ;
    }
}
