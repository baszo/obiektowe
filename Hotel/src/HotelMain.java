import resources.Configuration;
import services.ReservationService;
import services.RoomService;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Scanner;

public class HotelMain {
    RoomService roomService;
    Scanner in;

    public HotelMain()
    {
        this.roomService = new RoomService();
        this.in = new Scanner(System.in);
    }

    String menu = "Witam w hotelu XYZ \n" +
                "prosze wybrac jedna z opcji: \n" +
                "1) sprawdź oferte\n" +
                "2) wykonaj rezerwacje\n" +
                "q) aby wyjść";

    public void getPriceOfRoom()
    {
        DateFormat formatter = new SimpleDateFormat("dd.MM.yyyy");
        Date from = null,to = null;
        System.out.println("Podaj date początku interesującego Cię okresu w formie dzien.miesiac.rok");
        try {
            from = formatter.parse(in.nextLine());
            System.out.println(from);
        System.out.println("Podaj date końcową interesującego Cię okresu w formie dzien.miesiac.rok");
            to = formatter.parse(in.nextLine());
            System.out.println(to);
        }
        catch (Exception e){
            System.out.println("Błędny format");
        }
        System.out.println(roomService.getAllClases());
        Integer classId = Integer.parseInt(in.nextLine());
        System.out.println(roomService.getRoomCostPrice(from, to, classId - 1));

    }

    public void importConfig()
    {
        try {
            System.out.println(Configuration.getInstance()
                    .getProperty("discount"));
        }
        catch (Exception e){
            System.out.println(e.toString());
        }

    }

    public void setResertavion()
    {
        DateFormat formatter = new SimpleDateFormat("dd.MM.yyyy");
        Date from = null,to = null;
        ReservationService reservationService = new ReservationService();

        from = new Date("20.10.2001");
        to = new Date("25.10.2001");

        System.out.println(reservationService.makeReservation("krzysz s", "ss@ss.pl", from, to, 4, 2));

//        try {
//            System.out.println("Podaj date rezerwacji w formie dzien.miesiac.rok");
//            from = formatter.parse(in.nextLine());
//            System.out.println(from);
//            System.out.println("Podaj date rezerwacji w formie dzien.miesiac.rok");
//            to = formatter.parse(in.nextLine());
//            System.out.println(to);
//        }
//        catch (Exception e){
//            System.out.println("Błędny format");
//        }

        //System.out.println(reservationService.makeReservation(from, to));

    }

    public void run()
    {
        String input = "";

        try {
            importConfig();
            System.exit(1);
            do {

                System.out.println(menu);
                input = in.nextLine();
                switch (input) {
                    case "1": getPriceOfRoom(); break;
                    case "2": setResertavion(); break;
                }

            }
            while (input.compareTo("q") != 0);
        }
        catch(Exception e)
        {

        }

    }
}

class Start{
    public static void main(String[] args) {
        HotelMain hotel = new HotelMain();
        hotel.run();
    }
}
