package services;

import model.ClassOfRoom;
import model.Importer;
import model.Pricing;
import model.Room;

import java.util.*;


public class RoomService {
    Importer data;
    List<Room> Allrooms;
    List<Pricing> Allpricing;
    List<ClassOfRoom> AllClasses;
    Map<Integer,Integer> HotalResources;

    public RoomService(){
        data = new Importer();
        Allrooms = data.importRoomsFromCsv();
        Allpricing = data.importCsvDataPricingData();
        AllClasses = data.imporClassRoomsData();
    }

    public Map<Integer,Integer> getHotelResources()
    {
        Map<Integer,Integer> clasro = new HashMap<>();


        for(Room ro : Allrooms)
        {
            Integer roId = ro.getClassOfRoom().getId();
            Integer count = clasro.get(roId);
            if(count==null)
            {
                clasro.put(roId,1);
            }
            else
            {
                clasro.put(roId,count+1);
            }
        }

        return clasro;
    }

    public List<Integer> getRoomCostPrice(Date from, Date to, int classId){
        List<Integer> price = new ArrayList<>();
        for(Pricing e : Allpricing){
            if(e.getBegin().compareTo(from) > 0  || e.getEnd().compareTo(to) > 0)
            {
                price.add(e.getRooms().get(classId));
            }
        }
        return price;
    }
    public List<Integer> getRoomsAvalibleForClassId(Date from, Date to, int classId){
        List<Integer> rooms = new ArrayList<>();
        for(Room ro : Allrooms)
        {
            if(ro.getClassOfRoom().getId() == classId)
            {
                rooms.add(ro.getRoomNumber());
            }
        }

        return rooms;
    }
    
    public String getAllClases(){
        String desc = "";
        for(int i=1;i<AllClasses.size();i++)
        {
            desc += i+") "+ AllClasses.get(i).toString()+"\n";
        }
        return desc;
    }
}
