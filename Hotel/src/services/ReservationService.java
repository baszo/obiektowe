package services;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import model.Guest;
import model.Importer;
import model.Pricing;
import model.Reservation;


public class ReservationService {
    public String makeReservation(String name, String email, Date start_at, Date end_at, int numberOfPersons, int classOfRoom ) {

        boolean isAvailable = this.isAvailable(start_at, end_at, classOfRoom - 1);

        if (isAvailable) {
            Guest guest = new Guest(name, email);
            //Reservation reservation = new Reservation(guest, )

            return "Zarezerwowano";
        }

        return "nope";
    }

    public boolean isAvailable(Date start_at, Date end_at, int classOfRoom){

        //tutaj mamy tablice z wszyskimi pokojami
        RoomService roomService = new RoomService();
        List<Integer> all_rooms = roomService.getRoomCostPrice(start_at, end_at, classOfRoom-1);

        //tutaj mamy wszyskie rezerwacje w danym terminie
        List<Integer> reservations = this.getReservations(start_at, end_at);

        //odejmujemy jedne od drugich
        //List<Integer> room_available = all_rooms - reservations;

        return true;
    }

    private List<Integer> getReservations(Date start_at, Date end_at){
        Importer data = new Importer();
        List<Reservation> AllReservations = data.importReservationsFromCsv();

        List<Integer> reservation = new ArrayList<>();
        for(Reservation e : AllReservations){
            if(e.getBegin().compareTo(start_at) > 0  || e.getEnd().compareTo(end_at) > 0)
            {
                //reservation.add(e.getRooms());
            }
        }
        return reservation;

    }
}
