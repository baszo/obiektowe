/**
 * Created by andrzej on 05.10.15.
 */
class Auto {
    Silnik serce = new Silnik();
    KomputerPokladowy komp = new KomputerPokladowy();
    static Integer StanPaliwa = 0;
    static Integer przebieg = 0;

    void uruchom(){
        System.out.println(serce.start());
    }

    void getPrzebieg()
    {
        System.out.println(przebieg);
    }
    void wylacz(){
        System.out.println(serce.stop());
    }

    void zatankuj(Integer litry){
        StanPaliwa +=litry;
    }

    void jedz(Integer km){
        if(serce.czyWlaczony())
            przebieg +=km;
        else
            System.out.println("Silnik nie włączony");
    }

    public void zadzwon(String osoba) {
        System.out.println(komp.zadzwon(osoba));
    }

    public void zasieg() {
        System.out.println(komp.dystans(StanPaliwa).toString() + " km");
    }
}

class Silnik {

    boolean wlaczony = false;

    String start(){
        wlaczony = true;
        return "Uruchomiono";
    }

    String stop(){
        wlaczony = false;
        return "Stop";
    }

    Boolean czyWlaczony(){
        return wlaczony;
    }
}


class KomputerPokladowy {
    String zadzwon(String uzytkownik) {
        return "Dzwonie do " + uzytkownik;
    }

    Integer dystans(Integer paliwo)
    {
        return paliwo/8;
    }
}

class TestAuta {
    public static void main(String[] args) {
        Auto autko = new Auto();
        autko.uruchom();
        autko.jedz(50);
        autko.getPrzebieg();
        autko.wylacz();
        autko.zadzwon("Ziomek");
        autko.zatankuj(50);
        autko.zasieg();
    }
}